package windows;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Tabs {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.get("https://demoqa.com/browser-windows");
		   driver.manage().window().maximize();
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//button[text()='New Tab']")).click();
		Thread.sleep(2000);
		
		 Set<String> alltabs = driver.getWindowHandles();
		 
		 for(String tab : alltabs) {
			 driver.switchTo().window(tab);
			 Thread.sleep(5000);
			 String title = driver.getTitle();
			 System.out.println( title);	
			 driver.close();
		/*	if(title.equalsIgnoreCase("This is a sample page")) {
				String text = driver.findElement(By.id("sampleHeading")).getText();
				System.out.println("Tab title : " + text);
				
				driver.close();
			}
			*/
			
		 }
		
	}

}
