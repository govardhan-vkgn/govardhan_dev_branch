package windows;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChildWindow {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/browser-windows");

        // Open new child window within the main window
        driver.findElement(By.id("windowButton")).click();

        //Get handles of the windows
        String mainWindowHandle = driver.getWindowHandle();
       
        
        Set<String> allWindowHandles = driver.getWindowHandles();
        
        Iterator<String> iterator = allWindowHandles.iterator();

        // Here we will check if child window has other child windows and will fetch the heading of the child window
        while (iterator.hasNext()) {
            String ChildWindow = iterator.next();
            
            if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
            	driver.switchTo().window(ChildWindow);
            	 String text = driver.findElement(By.id("sampleHeading")).getText();
            	 System.out.println(text);
            	 Thread.sleep(5000);
                 driver.close();
                 Thread.sleep(5000);
            }
            
           
           
           
       //   driver.switchTo().window(mainWindowHandle).close();
               /* if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	                driver.switchTo().window(ChildWindow);
	                WebElement text = driver.findElement(By.id("sampleHeading"));
	                System.out.println("Heading of child window is " + text.getText());
                }*/
        }
    }
}
