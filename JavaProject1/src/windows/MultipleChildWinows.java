package windows;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleChildWinows {
	public static void main(String[] args) throws InterruptedException {									
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
	    driver.manage().window().maximize();
	    driver.get("https://demoqa.com/browser-windows");

	    // Opening all the child window
	   // driver.findElement(By.id("windowButton")).click();
	    driver.findElement(By.id("messageWindowButton")).click();
	    
	    String MainWindow = driver.getWindowHandle();
	    System.out.println("Main window handle is " + MainWindow);

	    // To handle all new opened window
	    Set<String> allwindows = driver.getWindowHandles();
	    System.out.println("Child window handle is" + allwindows);
	    Iterator<String> i1 = allwindows.iterator();

	    // Here we will check if child window has other child windows and when child window
	    //is the main window it will come out of loop.
	      while (i1.hasNext()) {
	          String ChildWindow = i1.next();
	          
	          if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
	              driver.switchTo().window(ChildWindow);
	            //  String text = driver.findElement(By.xpath("//body[contains(text(),'Knowledge')]")).getText();
	           //   System.out.println(text);
	              Thread.sleep(5000);
	              driver.close();
	              System.out.println("Child window closed");
	           }      
	          
	       }
	      
	      Thread.sleep(5000);
	      driver.switchTo().window(MainWindow).close();
	    }
}
