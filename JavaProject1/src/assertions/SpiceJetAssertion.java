package assertions;

import java.util.concurrent.TimeUnit;
import org.junit.Assert;  
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SpiceJetAssertion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
        driver.get("https://andhragreens.com/");
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        
        System.out.println(driver.getTitle());
        Assert.assertEquals( driver.getTitle(),"Buy Fruits, Groceries, Vegetables from Our Exclusive Online Store | Andhragreens");
        System.out.println("Application is launched successfully");
        
        driver.findElement(By.linkText("Login")).click();
        
        WebDriverWait expwait = new WebDriverWait(driver, 100);        
        expwait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//input[@formcontrolname='userId'])[1]")));  
        
        System.out.println("Test Started");    
        String text =  driver.findElement(By.xpath("//h2[@class='welcome-note selected-login']")).getText();
        System.out.println(text);
        
        Assert.assertEquals(text,"LOGIN AG");
        
        System.out.println("Test Pass");    
        
        
                
        /*
        System.out.println(driver.findElement(By.xpath("(//div[@class='mat-radio-container'])[1]")).isSelected());
        driver.findElement(By.xpath("(//div[@class='mat-radio-container'])[1]")).click();
        
        Assert.assertTrue(driver.findElement(By.xpath("(//div[@class='mat-radio-container'])[1]")).isSelected());
        System.out.println("Test is pass");        
        
        
		System.out.println("Test Started");    
        
        Assert.assertFalse(true);
        System.out.println("Test is pass");    
   */
        
        
	}

}
