package actionsCls;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TooltipDemo {
	public static void main(String[] args) { 
			
			
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		  
        driver.get("https://demoqa.com/tool-tips");
        driver.manage().window().maximize();
		
		
		Actions act = new Actions(driver);
		
		WebElement element = driver.findElement(By.id("tooltipDemo")); 
		
		act.moveToElement(element).perform();
		
		WebElement toolTip = driver.findElement(By.cssSelector(".tooltiptext")); 
		
		System.out.println(toolTip.getText());
		
		
		
		
	}
}
