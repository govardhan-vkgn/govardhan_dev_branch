package actionsCls;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;



public class CommonFunctions {
	
	WebDriver driver;
	
	public void LaunchBrowser(String Browser, String url) {
		
		if (Browser.equalsIgnoreCase("CHROME")) {
		
			System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
		}else if (Browser.equalsIgnoreCase("FIREFOX")) {
			System.setProperty("webdriver.gecko.driver", "Drivers/geckodriver");
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	
	
	
	
	
	
	
	public void enterValueIntoTexField(WebElement ele, String val) {
		ele.sendKeys(val);
	}

	public void clickElement(WebElement ele) {
		ele.click();
	}
	
	
	public void selectValueFromDropdown(WebElement ele, String text) {
		
		Select select = new Select(ele);		
		select.selectByVisibleText(text);
		
	}
	
	
}
