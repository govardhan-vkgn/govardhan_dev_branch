package actionsCls;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class KeybpardEvents extends CommonFunctions{

	public void VerifyKeyboardEventest () throws InterruptedException {

		
		LaunchBrowser("CHROME","https://demoqa.com");
		
		WebElement fullName = driver.findElement(By.id("userName"));
		enterValueIntoTexField( fullName, "Mr.Peter Haynes");		
		
        //Enter the Email
        WebElement email=driver.findElement(By.id("userEmail"));
        enterValueIntoTexField( email, "PeterHaynes@toolsqa.com");

        WebElement dropdown=driver.findElement(By.id("country"));
        selectValueFromDropdown( dropdown,  "INDIA");
        
        
	}

}
