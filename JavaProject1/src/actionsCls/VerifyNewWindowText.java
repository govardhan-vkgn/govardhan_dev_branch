package actionsCls;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class VerifyNewWindowText {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://demoqa.com/");
		Thread.sleep(5);
		
		System.out.println(driver.getPageSource());
		
		driver.navigate().to("");
		
		driver.findElement(By.xpath("//div[contains(text(),'Alerts, Frame & Windows')]")).click();
		
		if (driver.findElement(By.xpath("//div[contains(text(),'Alerts, Frame & Windows')]")).isSelected() == true) {
			System.out.println();
		}else {
			driver.findElement(By.xpath("//div[contains(text(),'Alerts, Frame & Windows')]")).click();
		}
		
		driver.findElement(By.xpath("//span[text()='Browser Windows']")).click();
		
		driver.findElement(By.id("tabButton")).click();
		
		Set<String> allwindows = driver.getWindowHandles();
		
		for ( String window:allwindows) {
			driver.switchTo().window(window);
			
			String text = driver.findElement(By.id("sampleHeading")).getText();
			
			if (text.equalsIgnoreCase("This is a sample page")) {
				System.out.println("My text case is pass");
				driver.close();
				break;
			}
			
			// Assert.assertEquals(text, "This is a sample page");
			
		}
		
		
		
		Actions act = new Actions(driver);
		

		
		
		
	}

}
