package actionsCls;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AActionCls {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		  
        driver.get("https://www.ebay.com/");
        driver.manage().window().maximize();
		
		
		Actions act = new Actions(driver);
		
		act.moveToElement(driver.findElement(By.id("gh-logo"))).build().perform();
		System.out.println("Move");
		Thread.sleep(5000);
		act.doubleClick(driver.findElement(By.id("gh-logo"))).build().perform();
		System.out.println("doubleClick");
		
	}

}
