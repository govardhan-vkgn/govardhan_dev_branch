package actionsCls;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.By;

import org.openqa.selenium.interactions.Actions;

public class RightClickDemo {

	public static void main(String[] args) throws InterruptedException {
		// Note: Following statement is required since Selenium 3.0,
		System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://demoqa.com/buttons");
		driver.manage().window().maximize();

		Actions actions = new Actions(driver);
		WebElement doubleClickBtn = driver.findElement(By.id("doubleClickBtn"));
		
		actions.doubleClick(doubleClickBtn).build().perform();
		
		Thread.sleep(5000);  
		
		WebElement rightClickBtn = driver.findElement(By.id("rightClickBtn"));
		actions.contextClick(rightClickBtn).build().perform();
		Thread.sleep(5000);  
		
	/*	
		actions.contextClick(btnElement).perform();
		System.out.println("Right click Context Menu displayed");

		WebElement elementOpen = driver.findElement(By.xpath(".//div[@id='rightclickItem']/div[1]"));
		elementOpen.click();

		driver.switchTo().alert().accept();
		System.out.println("Right click Alert Accepted");
*/
		driver.close();

	}

}
