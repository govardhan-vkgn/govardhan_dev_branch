package waits1;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class FluentWaitCls {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		  
        driver.get("https://demo.guru99.com/test/newtours/register.php");
        driver.manage().window().maximize();
    	
    	Wait wait = new FluentWait(driver)
    			.withTimeout(Duration.ofSeconds(100))
    			.pollingEvery(Duration.ofSeconds(10));
               // .ignoring(NoSuchElementException.class);
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("firstName")));      
        
    	//WebElement element = driver.findElement(By.name("lastName"));
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("lastName")));          		
	}

}
