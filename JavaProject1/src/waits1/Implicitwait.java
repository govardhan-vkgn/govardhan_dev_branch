package waits1;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Implicitwait {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);  //5 
		  
        driver.get("https://demo.guru99.com/test/newtours/register.php");
        driver.manage().window().maximize();
        
        driver.findElement(By.name("firstName")).sendKeys("Gova");        //15  20 (default) + 10 (implicit)
        
        WebDriverWait expwait = new WebDriverWait(driver, 100);
        
        expwait.until(ExpectedConditions.visibilityOfElementLocated(By.name("lastName")));      
        driver.findElement(By.name("lastName")).sendKeys("Gova");
       
        expwait.until(ExpectedConditions.visibilityOfElementLocated(By.name("phone"))); 
       
        		
	}

}
