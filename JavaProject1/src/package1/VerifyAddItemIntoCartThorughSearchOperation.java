package package1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VerifyAddItemIntoCartThorughSearchOperation extends BaseClass{

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver driver = BaseClass.InitiateDriver();		
		BaseClass.loginIntoApplication();
		Thread.sleep(5000);
		
		WebElement element = driver.findElement(By.id("mat-input-0"));
		element.sendKeys("fruits");
		
		
		driver.findElement(By.xpath("//div[@class='search-btn']")).click();
		System.out.println("Search criteria is succesfull ");
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//span[text()='Add'][1]")).click();
		System.out.println("the product is addded to cart");
		
	}
}
