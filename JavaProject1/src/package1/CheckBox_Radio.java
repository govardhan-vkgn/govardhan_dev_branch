package package1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import graphql.Assert;

public class CheckBox_Radio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
        driver.get("https://demo.guru99.com/test/radio.html");
    
         List<WebElement> listradiobtn = driver.findElements(By.xpath("//input[@type='radio']"));
        
         System.out.println(listradiobtn.size());
         
         for (WebElement ele:listradiobtn) {
        	 System.out.println(ele.getAttribute("value"));
        	 String test = ele.getAttribute("value");
        	if( test.equalsIgnoreCase("Option 2")) {
        		ele.click();
        	}
         }
         
        
        WebElement checkbox =  driver.findElement(By.xpath("//input[@type='checkbox']"));
        if (checkbox.isSelected()) {
        	checkbox.click();
        }
        
         List<WebElement> listcheckbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
        
         System.out.println(listcheckbox.size());
         for(WebElement ele : listcheckbox) {
        	ele.click();
        }
        
        Assert.assertTrue(driver.findElement(By.xpath("//input[@value='checkbox1']")).isSelected());
 		
         
        if (driver.findElement(By.xpath("//input[@value='checkbox1']")).isSelected() == false) {
        	driver.findElement(By.xpath("//input[@value='checkbox1']")).click();
        }
      
        driver.findElement(By.xpath("//input[@value='checkbox2']")).click();
        driver.findElement(By.xpath("//input[@value='checkbox3']")).click();
        
        
        
	}

}
