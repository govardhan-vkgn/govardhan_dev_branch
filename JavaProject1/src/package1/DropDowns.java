package package1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDowns {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
        driver.get("https://demo.guru99.com/test/newtours/register.php");
        
//        driver.findElement(By.name("country")).click();
//        driver.findElement(By.xpath("//option[@value='INDIA']")).click();
//        driver.findElement(By.xpath("//option[@value='INDIA']")).sendKeys(Keys.ESCAPE);
        
        
        WebElement drpCountry = driver.findElement(By.name("country"));
        
        
        Select sel = new Select(drpCountry);
        sel.selectByIndex(1);        
        Thread.sleep(3000);
        sel.selectByValue("IND");   // select the value which are avialble in select tab     
        Thread.sleep(3000);        
        sel.selectByVisibleText("IND"); //based on the options / list will be selecting the values 
      
        
	}

}
