package package1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class VerifyAddItemIntoCartFromAllCategories extends BaseClass {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver driver = BaseClass.InitiateDriver();		
		BaseClass.loginIntoApplication();
		
		if (driver.findElement(By.xpath("//span[text()='All categories']")).isDisplayed()) {
			driver.findElement(By.xpath("//span[text()='All categories']")).click();
		}
		
		
		driver.findElement(By.xpath("//span[text()='fruits & vegetables']")).click();
		Thread.sleep(5000);
	
		driver.findElement(By.xpath("//span[text()='Add'][1]")).click();
		System.out.println("the product is addded to cart");
	}

}
