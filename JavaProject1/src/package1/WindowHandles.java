package package1;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		  
        driver.get("https://demoqa.com/browser-windows");
        driver.manage().window().maximize();
        
        driver.findElement(By.id("messageWindowButton")).click();
        
        Set<String> alltabs = driver.getWindowHandles();
		for(String tab : alltabs) {
			 driver.switchTo().window(tab);
			 System.out.println( driver.getTitle());			 
		}
	}

}
