package package1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TabsInBrowser {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		  
        driver.get("https://www.ebay.com/");
        driver.manage().window().maximize();
        
        driver.findElement(By.id("gh-ac")).sendKeys("Apple iPhone XR 128GB");
        driver.findElement(By.id("gh-btn")).click();
      
        ////h3[contains(text(),'Apple iPhone XR BLACK - 128GB')]   
        
        Thread.sleep(5000);        
        List<WebElement> allPhones = driver.findElements(By.xpath("//h3[@class='s-item__title']"));
       
        
        System.out.println(allPhones.size());
        Thread.sleep(5000);      
		for(WebElement ele : allPhones) {
        	ele.click();
        	Thread.sleep(5000);     
        	ArrayList<String> alltabs = new ArrayList<String> (driver.getWindowHandles());
            driver.switchTo().window(alltabs.get(0));
            Thread.sleep(5000);     
        }
        
		Set<String> alltabs = driver.getWindowHandles();
		for(String tab : alltabs) {
			 driver.switchTo().window(tab);
			 System.out.println( driver.getTitle());
			 
			 
			 
			 driver.close();
			 
		}
      
		/*
		 * 
		 * 
		 
		 
		 Task : sorting
	
		apple iphone xr 128gb | eBay
		Apple iPhone XR BLACK - 128GB - (Unlocked) A1984 (CDMA + GSM) | eBay
		Apple iPhone XR - 128GB - Coral Color W/ Free Extra's (T-Mobile Unlocked) A1984 | eBay
		Apple iPhone XR 128GB, Black Factory Unlocked (CDMA + GSM) Ex+ | eBay
		New Sealed Apple iPhone XR 128GB Black Factory Unlocked Fast Shipping ! | eBay
		New Sealed Apple iPhone XR 128GB Blue Factory Unlocked Fast Shipping ! | eBay
		NEW Sealed Apple iPhone XR 128GB Yellow Factory Unlocked Fast Shipping ! | eBay


		 */
        
	}

}
