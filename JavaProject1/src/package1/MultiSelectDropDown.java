package package1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MultiSelectDropDown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
        driver.get("https://demoqa.com/select-menu");
		
		WebElement multidrp = driver.findElement(By.xpath("//select[@name='cars']"));
	
		Select sel = new Select(multidrp);
		
		sel.selectByIndex(0);
		
		sel.selectByIndex(1);
		sel.selectByVisibleText("Audi");
		sel.selectByVisibleText("Opel");
		
		
		Thread.sleep(5000);
		sel.deselectAll();
		
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys("Green");
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys(Keys.ENTER);
//		
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys("Blue");
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys(Keys.ENTER);
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys(Keys.ENTER);
//		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[3]")).sendKeys(Keys.ENTER);
	}

}
