package package1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsClass {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		WebDriver driver = new ChromeDriver();
        driver.get("http://demo.guru99.com/test/delete_customer.php");
        Thread.sleep(5000);
        
        driver.findElement(By.name("submit")).click();
        
        Thread.sleep(5000);
        
     
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		Thread.sleep(5000);
		
		Alert al = driver.switchTo().alert();
		//driver.switchTo().alert().dismiss();
		al.accept();
		
		Thread.sleep(5000);
		String text1 = driver.switchTo().alert().getText();
		System.out.println(text1);
		driver.switchTo().alert().accept();
		//al.dismiss();
		
		
	}

}
