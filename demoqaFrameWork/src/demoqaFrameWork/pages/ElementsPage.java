package demoqaFrameWork.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.CommonFunctions;

public class ElementsPage {

	final WebDriver driver;

	CommonFunctions commonfunction = new CommonFunctions();
	
	@FindBy(xpath = "//div[@class='main-header']")
	WebElement ElementsPageTitle;

	@FindBy(xpath = "//span[text()='Text Box']")
	WebElement TextBox;
	
	@FindBy(id = "userName")
	WebElement userName;
	
	// Constructor, as every page needs a Webdriver to find elements
	public ElementsPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

	public void verifyElementsPage() {
		System.out.println(ElementsPageTitle.isDisplayed());
		String text = ElementsPageTitle.getText();
		
		Assert.assertEquals(text, "Elements");
	}
	
	public void clickTextBox() {
		commonfunction.clickElement(TextBox);
	}
	
	
	public void enterFullName(String val) {
		commonfunction.enterValueIntoTexField(userName, val);
	}
	
	

}
