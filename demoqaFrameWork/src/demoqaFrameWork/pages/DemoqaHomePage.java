package demoqaFrameWork.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import common.CommonFunctions;
import graphql.Assert;

public class DemoqaHomePage {

	final WebDriver driver;

	CommonFunctions commonfunction = new CommonFunctions();
	
	@FindBy(xpath="//h5[text()='Elements']")
	WebElement HomePageTitle;
	
	@FindBy(xpath="//h5[text()='Elements']")
	WebElement Elements;
	
	@FindBy(xpath="//h5[text()='Forms']")
	WebElement Forms;
	
	@FindBy(xpath="//h5[text()='Alerts, Frame & Windows']")
	WebElement AlertsFrameWindow;
	
	@FindBy(xpath="//h5[text()='Widgets']")
	WebElement Widgets;
	
	// Constructor, as every page needs a Webdriver to find elements
	public DemoqaHomePage(WebDriver driver) {
		this.driver = driver; // sets the driver argument to local driver variable
		PageFactory.initElements(driver, this); // it calls the testng annotation like @FindBy
	}

	public void verifyHomePage() {
		System.out.println(HomePageTitle.isDisplayed());
		Assert.assertTrue(HomePageTitle.isDisplayed());
	}
	
	public void navigateToElements() {
		commonfunction.clickElement(Elements);
	}
	
	
	
	
}
