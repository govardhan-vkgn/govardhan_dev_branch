package demoqaFrameWork.testcases;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import common.BaseClass;
import demoqaFrameWork.pages.DemoqaHomePage;
import demoqaFrameWork.pages.ElementsPage;

public class VerifyElementsTestCases extends BaseClass{

	WebDriver driver;
	DemoqaHomePage demoqaHomePage = new DemoqaHomePage(driver);
	ElementsPage elementsPage = new ElementsPage(driver);
	
	@Test
	public void verifyTextBox() {
		LaunchBrowser( "CHROME",  "https://demoqa.com/");
		demoqaHomePage.verifyHomePage();
		demoqaHomePage.navigateToElements();
		elementsPage.clickTextBox();
		elementsPage.enterFullName("malli") ;
	}

	@Test
	public void verifyCheckBox() {

	}

	@Test
	public void verifyRadioButton() {

	}

}
