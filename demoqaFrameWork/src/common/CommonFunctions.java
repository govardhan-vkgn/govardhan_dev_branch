package common;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CommonFunctions {
	
	public void enterValueIntoTexField(WebElement ele, String val) {
		ele.sendKeys(val);
	}

	public void clickElement(WebElement ele) {
		ele.click();
	}
	
	
	public void selectValueFromDropdown(WebElement ele, String text) {
		Select select = new Select(ele);		
		select.selectByVisibleText(text);
	}
	
	
}
