Feature: ebay "Add to Cart" feature

  Background:
    Given the user is on ebay home page

@TC001 @smoke @All
  Scenario: Adding the required product to the cart in ebay
    And the user searches for "Ninja SP101 Foodi 8-in-1 Digital Air Fry Oven" using search bar
    When the user selects the product from the list of search results
    Then the product details should be displayed
    When the user selects the Add to Cart button
    Then the product should be added to the cart
@TC002 @Regression @All
  Scenario Outline: Adding the required product to the cart in ebay
    And the user searches for "<Product Name>" using search bar
    When the user selects the product from the list of search results
    Then the product details should be displayed
    When the user selects the Add to Cart button
    Then the product should be added to the cart

    Examples:
      | Product Name                                                |
     # | Apple iPhone 8 Plus 64GB Factory Unlocked AT&T Verizon T-Mobile Unlocked               |
      | Ninja SP101 Foodi 8-in-1 Digital Air Fry Oven               |
      | Gourmet Footwear the 35 lite Tan Croc Size 9 EXTREMELY RARE |
    
@TC003 @Regression @All
  Scenario Outline: Adding the required product to the cart in ebay
    And the user searches for "<Product Name>" using search bar
    When the user selects the product from the list of search results
    Then the product details should be displayed
    Then the product details should be selected
    When the user selects the Add to Cart button
    Then the product should be added to the cart

    Examples:
      | Product Name                                                |
      | Apple iPhone 8 Plus 64GB Factory Unlocked AT&T Verizon T-Mobile Unlocked               |
      
@TC004  @smoke @All
Scenario: Adding the required product to the cart by selecting category
    When the user select the product form drop down "Cell Phones & Accessories" and click on search
    Then the user select the "Cell Phones & Smartphones" from the list of search results
    Then click on the "Apple iPhone 11 pro max 64gb AT&T Midnight Green Hairline"
    When the user selects the Add to Cart button
    Then the product should be added to the cart
        
      