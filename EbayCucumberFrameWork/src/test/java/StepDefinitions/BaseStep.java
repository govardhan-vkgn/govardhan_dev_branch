package StepDefinitions;

import Utils.LoggerUtils;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public abstract class BaseStep {

    HashMap<String, Object> pageObjectMappings = new HashMap<>();

    public <T> T getPageInstance(WebDriver driver, Class<T> classInstance) {

        try {
            pageObjectMappings.put(classInstance.getName(), classInstance.getDeclaredConstructor(WebDriver.class).newInstance(driver));
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException e) {
            LoggerUtils.logError(
                    "getPage() method thrown exception while getting " + classInstance.getName() + " instance", e);
        }

        return classInstance.cast(pageObjectMappings.get(classInstance.getName()));
    }


}
