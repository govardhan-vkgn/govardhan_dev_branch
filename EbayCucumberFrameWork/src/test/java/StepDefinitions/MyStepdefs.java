package StepDefinitions;

import AppHooks.ApplicationHooks;
import Domain.Product;
import Pages.HomePage;
import Pages.ProductDetails;
import Pages.SearchResults;
import Pages.ShoppingCartDetails;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class MyStepdefs extends BaseStep {

    private WebDriver driver;
    private HomePage homePage;
    private SearchResults searchResults;
    private ProductDetails productDetails;
    private ShoppingCartDetails shoppingCartDetails;
    private Product product;

    public MyStepdefs(ApplicationHooks applicationHooks) {
        this.driver = applicationHooks.getDriver();
        this.homePage = getPageInstance(driver, HomePage.class);
        this.searchResults = getPageInstance(driver, SearchResults.class);
        this.productDetails = getPageInstance(driver, ProductDetails.class);
        this.shoppingCartDetails = getPageInstance(driver, ShoppingCartDetails.class);
    }

    @Given("the user is on ebay home page")
    public void the_user_is_on_ebay_home_page() {
        homePage.isSearchBarExist();
    }

    @Given("the user searches for {string} using search bar")
    public void the_user_searches_for_using_search_bar(String searchProductName) {
        product = new Product(searchProductName);
        homePage.enterSearchWord(searchProductName);
        homePage.clickOnSearchButton();
    }

    @When("the user selects the product from the list of search results")
    public void theUserSelectsTheProductFromTheListOfSearchResults() {
        searchResults.verifySearchResultExist(product.getProductName());
        searchResults.clickOnSearchResultLink(product.getProductName());
    }

    @Then("the product details should be displayed")
    public void theProductDetailsShouldBeDisplayed() {
        productDetails.verifyProductName(product.getProductName());
    }
    
    @Then("the product details should be selected")
    public void theProductDetailsShouldBeSelected() {
        productDetails.selectProductDetailsAsCapacity();
        productDetails.selectProductDetailsAsColor();    	
    }
    
    @When("the user selects the Add to Cart button")
    public void the_user_selects_the_Add_to_Cart_button() {
        productDetails.clickAddToCartButton();
    }

    @Then("the product should be added to the cart")
    public void the_product_should_be_added_to_the_cart() {
        shoppingCartDetails.verifyShoppingCartPageIsDisplayed();
        shoppingCartDetails.verifyProductNameInCart(product.getProductName());
        shoppingCartDetails.clickOnRemoveButton();
    }
    //==========================================================
    @When("the user select the product form drop down {string} and click on search")
    public void the_user_select_the_product_form_drop_down(String searchProductName) {
    	
        homePage.clickOnDropDown();
        homePage.selectDropDownVal();
        homePage.clickOnSearchButton();
     
    }
    
    @Then("the user select the {string} from the list of search results")
    public void the_user_select_the_from_the_list_of_search_results(String string) {
    	searchResults.clickItem();
    	productDetails.selecAppletItem();
    }

    @Then("click on the {string}")
    public void click_on_the(String searchProductName) {
    	product = new Product(searchProductName);
    	productDetails.clickOnAppleiPhone11();
    	
    }
}
