package Pages;

import Utils.WebDriverActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class HomePage {
    private WebDriver driver;
    private WebDriverActions actions;
    private By searchBar = By.id("gh-ac");
    private By searchButton = By.id("gh-btn");
   
    private By searchDropDown = By.id("gh-cat-td");
    private By selectDropDownVal = By.xpath("//select[@id='gh-cat']/option[text()='Cell Phones & Accessories']");
    
    public HomePage(WebDriver webDriver) {
        this.driver = webDriver;
        this.actions = new WebDriverActions(driver);
    }

    
    public void clickOnDropDown() {
    	actions.findElement(searchDropDown).click();
    }
    
    public void selectDropDownVal() {
    	actions.findElement(selectDropDownVal).click();
    }
    
    public void isSearchBarExist() {
        assertTrue(actions.findElement(searchBar).isDisplayed());
    }

    public void enterSearchWord(String searchProduct) {
        actions.findElement(searchBar).sendKeys(searchProduct);
    }

    public void clickOnSearchButton() {
        actions.findElement(searchButton).click();
    }

}
