package Pages;

import Utils.WebDriverActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ProductDetails {
    private WebDriver driver;
    private WebDriverActions actions;
    private By searchProductName = By.id("itemTitle");
  
    private By selectCapacity = By.xpath("//select[@id='msku-sel-1']");
    private By selectCapacityVal = By.xpath("//select[@id='msku-sel-1']/option[text()='64GB']");
    
    private By selectColor = By.xpath("//select[@id='msku-sel-2']");
    private By selectColorVal = By.xpath("(//select[@id='msku-sel-2']/option)[2]");
    
    private By addToCartButton = By.id("isCartBtn_btn");
    private By selecAppletItem = By.xpath(" //p[text()='Apple']");
  
    private By AppleiPhone11 = By.xpath("//h3[contains(text(),'Apple iPhone 11 pro max 64gb AT&T Midnight Green H')]");
    
    public ProductDetails(WebDriver webDriver) {
        this.driver = webDriver;
        this.actions = new WebDriverActions(driver);
    }
    
    
    public void selectProductDetailsAsCapacity() {
        actions.findElement(selectCapacity).click();
        actions.findElement(selectCapacityVal).click();
    }
    public void selectProductDetailsAsColor() {
        actions.findElement(selectColor).click();
        actions.findElement(selectColorVal).click();
    }
    
    public void clickOnAppleiPhone11() {
        actions.findElement(AppleiPhone11).click();
    }
    
    
    public void selecAppletItem() {
        actions.findElement(selecAppletItem).click();
    }

    public void verifyProductName(String expectedProductName) {
        Set<String> windows = driver.getWindowHandles();
        String currentWindowHandle = driver.getWindowHandle();
        for (String windowHandleId : windows) {
            if (!windowHandleId.equalsIgnoreCase(currentWindowHandle)) {
                driver.switchTo().window(windowHandleId);
                String searchResultProductName = actions.findElement(searchProductName).getText();
                assertEquals(expectedProductName, searchResultProductName);
            }
        }
    }

    public void clickAddToCartButton() {
        actions.findElement(addToCartButton).click();
    }

}
