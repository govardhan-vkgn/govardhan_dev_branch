package Pages;

import Utils.WebDriverActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ShoppingCartDetails {
    private WebDriver driver;
    private WebDriverActions actions;

    private By shoppingCartHeading = By.xpath("//h1[contains(text(),'Shopping cart')]");
    private By productNameInCart = By.xpath("//span[@class='BOLD']");
    private By removeItemFromCart = By.xpath("//button[@data-test-id='cart-remove-item']");

    public ShoppingCartDetails(WebDriver webDriver) {
        this.driver = webDriver;
        this.actions = new WebDriverActions(driver);
    }

    public void verifyShoppingCartPageIsDisplayed() {
        assertTrue(actions.findElement(shoppingCartHeading).isDisplayed());
    }

    public void verifyProductNameInCart(String expectedProductName) {
        String actualProductName = actions.findElement(productNameInCart).getText();
        assertEquals(expectedProductName, actualProductName);
    }

    public void clickOnRemoveButton() {
        actions.findElement(removeItemFromCart).click();
    }

}

