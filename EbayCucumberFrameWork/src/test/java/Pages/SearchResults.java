package Pages;

import Utils.WebDriverActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class SearchResults {

    private WebDriver driver;
    private WebDriverActions actions;

    private By searchResult = By.xpath("//h3[text()='%s']");
    private By saveSearchResultToolTip = By.xpath("//div[@class ='srp-save-search__tooltip srp-save-search__tooltip--shown']");
    private By searchResultLink = By.xpath("//h3[text()='%s']/parent::a");
    private By saveSearchResultToolTipCloseButton = By.xpath("//button[@title='Close tooltip']");
    private By selectItem = By.xpath("//div[contains(text(),'Cell Phones & Smartphones')]");
    
    
    public SearchResults(WebDriver webDriver) {
        this.driver = webDriver;
        this.actions = new WebDriverActions(driver);
    }
    
    public void clickItem() {
        actions.findElement(selectItem).click();
    }


    public void verifySearchResultExist(String productName) {
        By locator = actions.convertStringToByClass(String.format(searchResult.toString(), productName));
        assertTrue(actions.findElement(locator).isDisplayed());
    }

    public void clickOnSearchResultLink(String productName) {
        if (actions.findElement(saveSearchResultToolTip).isDisplayed()) {
            actions.findElement(saveSearchResultToolTipCloseButton).click();
        }
        actions.findElement(actions.convertStringToByClass(String.format(searchResultLink.toString(), productName))).click();
    }
}
