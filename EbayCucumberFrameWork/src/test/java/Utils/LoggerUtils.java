package Utils;

import org.apache.log4j.Logger;

public class LoggerUtils {

    private static Logger Log = Logger.getLogger(LoggerUtils.class.getName());

    public static void logInfo(String message) {
        Log.info(message);
    }

    public static void logWarn(String message) {
        Log.warn(message);
    }

    public static void logError(String message, Exception exception) {
        Log.error(message, exception);
    }

    public static void fatal(String message) {
        Log.fatal(message);
    }

    public static void debug(String message) {
        Log.debug(message);
    }

}
