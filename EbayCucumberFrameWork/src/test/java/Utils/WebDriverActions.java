package Utils;

import io.cucumber.java.Scenario;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class WebDriverActions {

    private WebDriver driver;

    public WebDriverActions(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement findElement(By element) {
        try {
            return driver.findElement(element);
        } catch (Exception exception) {
            LoggerUtils.logError("Element Not Found", exception);
        }
        return null;
    }

    public By convertStringToByClass(String locator) {
        By loc = null;
        if (locator.contains("id"))
            loc = By.id(locator.substring(locator.indexOf("/"), locator.length() - 2));

        else if (locator.contains("name"))
            loc = By.name(locator.substring(locator.indexOf("/"), locator.length() - 2));

        if (locator.contains("xpath"))
            loc = By.xpath(locator.substring(locator.indexOf("/")));

        return loc;
    }

    public void takeScreenShot(Scenario scenario) {
        try {
            Screenshot myScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
            ImageIO.write(myScreenshot.getImage(), "PNG", new File("./target/" + scenario.getName() + ".png"));
        } catch (IOException exception) {
            LoggerUtils.logError("Unable to take the Screenshot", exception);
        }
    }

}
