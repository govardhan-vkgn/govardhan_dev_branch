package AppHooks;

import Utils.ConfigReader;
import Utils.WebDriverActions;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;

import Factory.DriverFactory;

import java.io.IOException;
import java.util.Properties;

public class ApplicationHooks {

    private DriverFactory driverFactory;
    private WebDriver driver;
    private ConfigReader configReader;
    private WebDriverActions actions;
    private Scenario scenario;
    Properties properties;
    private ThreadLocal<WebDriver> threadLocal = new ThreadLocal<WebDriver>();

    @Before
    public void launchBrowser(Scenario scenario) {
        this.scenario = scenario;
        configReader = new ConfigReader();
        driverFactory = new DriverFactory();
        properties = configReader.getProperty();
        driver = driverFactory.initialiseDriver(properties.getProperty("browser"));
        threadLocal.set(driver);
        driver.get(properties.getProperty("url"));
    }

    public WebDriver getDriver() {
        return threadLocal.get();
    }

    @After
    public void quitBrowser() throws IOException {
        if (scenario.isFailed()) {
            actions = new WebDriverActions(getDriver());
            actions.takeScreenShot(scenario);
        }
        driver.quit();
    }

}
